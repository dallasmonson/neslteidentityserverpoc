﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using IdentityModel.Client;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;

namespace APIPasswordClient
{
    public class Program
    {
        private const string identityServerURI = "http://localhost:5000"; // NOTE: MUST MATCH DNS CASE
        private const string clientCredentials = "api1";
        private const string apiURI = "http://localhost:5001/identity";
        private const string serviceAccountName = "alice";
        private const string serviceAccountPassword = "password";

        public static void Main(string[] args) => MainAsync().GetAwaiter().GetResult();

        private static async Task MainAsync()
        {
            // discover endpoints from metadata

            var discoClient = new DiscoveryClient(identityServerURI);
            discoClient.Policy.RequireHttps = false;

            var disco = await discoClient.GetAsync();
            if (disco.IsError)
            {
                Console.WriteLine(disco.Error);
                return;
            }

            // request token using service acount name and password instead of just the bearer token
            var tokenClient = new TokenClient(disco.TokenEndpoint, "ro.client", "secret");
            var tokenResponse = await tokenClient.RequestResourceOwnerPasswordAsync(serviceAccountName, serviceAccountPassword, "api1");

            if (tokenResponse.IsError)
            {
                Console.WriteLine(tokenResponse.Error);
                return;
            }

            Console.WriteLine(tokenResponse.Json);
            Console.WriteLine("\n\n");
            // call api
            var client = new HttpClient();
            client.SetBearerToken(tokenResponse.AccessToken);

            var response = await client.GetAsync(apiURI);
            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine(response.StatusCode);
                Console.Read();
            }
            else
            {
                var content = await response.Content.ReadAsStringAsync();
                Console.WriteLine(JArray.Parse(content));
                Console.Read();
            }
        }
    }
}
