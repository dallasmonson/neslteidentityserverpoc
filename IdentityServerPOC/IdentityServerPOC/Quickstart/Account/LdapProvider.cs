﻿using IdentityServer4.Test;
using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace IdentityServerPOC.Quickstart.Account
{
    public class LdapProvider
    {

        public bool ValidateActiveDirectoryLogin(string Domain, string Username, string Password)

        {
            bool Success = false;
            System.DirectoryServices.DirectoryEntry Entry = new System.DirectoryServices.DirectoryEntry(("LDAP://" + Domain), Username, Password);
            System.DirectoryServices.DirectorySearcher Searcher = new System.DirectoryServices.DirectorySearcher(Entry);
            Searcher.SearchScope = System.DirectoryServices.SearchScope.OneLevel;
            try
            {
                System.DirectoryServices.SearchResult Results = Searcher.FindOne();
                Success = !(Results == null);
            }
            catch (System.Exception)
            {

            }

            return Success;

        }

        public SearchResult FindLdapUser(string Domain, String Username, String Password)
        {
            TestUser ldapUser = new TestUser();
            System.DirectoryServices.DirectoryEntry Entry = new System.DirectoryServices.DirectoryEntry(("LDAP://" + Domain), Username, Password);
            System.DirectoryServices.DirectorySearcher Searcher = new System.DirectoryServices.DirectorySearcher(Entry);
            Searcher.SearchScope = System.DirectoryServices.SearchScope.OneLevel;
  
            SearchResult Results = Searcher.FindOne();
            return Results;

        }

        public String UserPin()
        {
            string Pin = "12345";
            //Add in logic to retrieve the pin from data store....
            return Pin;
        }
    }
}
